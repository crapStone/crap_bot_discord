use thiserror::Error;

#[derive(Debug, Error)]
pub enum CrapBotError {
    #[error("insufficient permissions")]
    InsufficientPermissions,

    #[error("no entry")]
    NoEntry,

    #[error("Error: {0}")]
    ErrorWithReply(String),

    #[error("Error: {0}")]
    ErrorWithMessage(String),

    #[error("database returned error: '{0}'")]
    SQLiteError(rusqlite::Error),

    #[error("discord returned error: '{0}'")]
    DiscordError(serenity::Error),
}

impl From<rusqlite::Error> for CrapBotError {
    fn from(err: rusqlite::Error) -> Self {
        use rusqlite::Error;
        match err {
            Error::QueryReturnedNoRows => CrapBotError::NoEntry,
            _ => CrapBotError::SQLiteError(err),
        }
    }
}

impl From<serenity::Error> for CrapBotError {
    fn from(err: serenity::Error) -> Self {
        CrapBotError::DiscordError(err)
    }
}

pub type Result<T> = std::result::Result<T, CrapBotError>;
