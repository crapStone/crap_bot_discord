begin;

create table if not exists admins (
    role_id integer not null,
    guild_id integer not null,
    primary key (role_id, guild_id)
);

create table if not exists created_channels (
    id integer primary key,
    guild_id integer not null,
    creator_id integer not null,
    parent_id integer not null
);

create table if not exists emoji_roles (
    id integer primary key,
    guild_id integer not null,
    message_id integer not null,
    emoji text not null,
    role_id integer not null
);

commit;
