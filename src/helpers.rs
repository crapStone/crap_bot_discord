use std::fmt::Display;

use serenity::{
    client::Context,
    model::channel::{Message, ReactionType},
};

use crate::{db, error::Result};

#[allow(dead_code)]
pub async fn unimplemented(ctx: &Context, msg: &Message) {
    reply_message(
        ctx,
        msg,
        "this functionality is currently under development",
    )
    .await;
}

pub async fn reply_message(ctx: &Context, msg: &Message, message: impl Display) {
    if let Err(why) = msg.reply(ctx, message).await {
        eprintln!("replying failed: {:?}", why);
    }
}

pub async fn user_is_admin(ctx: &Context, msg: &Message) -> bool {
    if let Some(guild_id) = msg.guild_id {
        if msg.author.id == guild_id.to_guild_cached(&ctx).await.unwrap().owner_id {
            println!("guild owner");
            return true;
        }
        let admin_roles;
        {
            admin_roles = match db::get_admins_for_guild(guild_id) {
                Ok(ids) => ids,
                Err(why) => {
                    eprintln!("an error occurred: {}", why);
                    eprintln!(
                        "guild has no admin role set: {}",
                        guild_id.name(ctx).await.unwrap()
                    );
                    return false;
                }
            };
        }
        for role in admin_roles {
            if check_role(msg, ctx, guild_id, &role).await {
                return true;
            }
        }
        false
    } else {
        eprintln!("admin check outside of guild");
        false
    }
}

pub async fn check_for_admin_else_reply(ctx: &Context, msg: &Message) -> Result<bool> {
    if user_is_admin(&ctx, &msg).await {
        Ok(true)
    } else {
        println!("user is not an admin");
        msg.react(&ctx, ReactionType::Unicode("❌".into())).await?;
        Ok(false)
    }
}

async fn check_role(
    msg: &Message,
    ctx: &Context,
    guild_id: serenity::model::id::GuildId,
    role: &serenity::model::id::RoleId,
) -> bool {
    match msg.author.has_role(&ctx, guild_id, role).await {
        Ok(is_admin) => is_admin,
        Err(why) => {
            eprintln!("admin check failed: {:?}", why);
            false
        }
    }
}
