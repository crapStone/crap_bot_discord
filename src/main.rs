mod db;
mod error;
mod handler;
mod helpers;

use std::env;

use handler::Handler;
use serenity::Client;

#[tokio::main]
async fn main() {
    println!(
        "starting crap Discord bot version {} ...",
        env!("CARGO_PKG_VERSION")
    );

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let mut client = Client::builder(&token)
        .event_handler(Handler::new())
        .await
        .expect("Err creating client");

    if let Err(why) = client.start().await {
        eprintln!("Client error: {:?}", why);
    }
}
