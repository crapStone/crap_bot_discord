# Craps Discord Bot

This bot is a hobby project and mainly used on our discord server.  
If you want to use it please host it yourself, because it was not written with performance in mind.

## Usage

### User commands

| command | explanation |
| --- | --- |
| `!create <name>` | creates a new channel |
| `!cleanup`, `!delete` | must be used in a created channel, deletes all created channels |

### Admin Commands

| command | explanation |
| --- | --- |
| `!info` | prints some information about guild, channel and roles, if it is a reply to a message also the message id |
| `!add_admin <role mention>` | add the mentioned role to the server admins |
| `!del_admin <role mention>` | not implemented |
| `!add_role <emoji> <role mention>` | must be a reply to a message, the bot reacts with the specified emoji and if a user reacts with the same emoji they will be added to the role |
| `!del_role <emoji> <role mention>` | not implemented |
